#include <iostream>

const int ERROR_NUM = 8200;

/**
 * @brief basic class representing clearance exception
 * (number must not be 8200)
*/
struct clearance_exception : public std::exception
{
    virtual const char* what() const throw() override
    {
        return "[!] number 8200 is not allowed";
    }
};

/**
 * @brief checks if one number is valid and throws exception otherwise
 * @param num - given number
*/
void check_valid(const int num)
{
    if (num == ERROR_NUM) throw clearance_exception();
}

/**
 * @brief checks if two numbers are both valid and throws exception otherwise
 * @param num1 - first number
 * @param num2 - second number
*/
void check_valid(const int num1, const int num2)
{
    check_valid(num1);
    check_valid(num2);
}


int add(int a, int b) {
    check_valid(a , b);
	return a + b;
}

int  multiply(int a, int b) {
    check_valid(a, b);
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a);
    check_valid(sum);
  }
  return sum;
}

int  pow(int a, int b) {
    check_valid(a, b);
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
    check_valid(exponent);
  }
  return exponent;
}

int main() {
	try
	{
        std::cout << pow(5, 5) << std::endl;
	}
    catch (clearance_exception& e)
    {
        std::cout << "[!] MyException caught" << std::endl;
        std::cout << e.what() << std::endl;
    }
	catch (std::exception& e)
	{
        std::cout << "[!] Other exception occurred" << std::endl;
        std::cout << e.what() << std::endl;
	}
 }


