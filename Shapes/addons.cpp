#include "addons.h"
#include "exceptions.h"

#include <sstream>


// helper functions


void addons::is_number(const std::string& s)
{
	// get iterator
	std::string::const_iterator it = s.begin();
	// go trough string
	while (it != s.end() && std::isdigit(*it)) ++it;

	// check if there is any non digit char
	if (!(!s.empty() && it == s.end())) throw inputException();
}


int addons::to_int(const std::string& str)
{
	// get string stream
	std::stringstream sstream(str);
	// create variable for the result
	int result;
	// convert string to int
	sstream >> result;

	return result;
}


int addons::get_num(std::string& message, int min_value, int max_value)
{

	std::string input;

	// get new input until it isn't correct
	while (true)
	{
		// variable for user input
		input = get_string(message);
		try
		{
			is_number(input);
		}
		catch (const inputException& e)
		{
			std::cout << e.what() << std::endl;
			continue;
		}
		break;
	}

	// get new input untill it's valid
	if (to_int(input) < min_value or to_int(input) > max_value)
	{
		std::cout << "only numbers between " << min_value << " and " << max_value << " are allowed" << std::endl;
		return get_num(message, min_value, max_value);
	}
	return to_int(input);
}


std::string addons::get_string(std::string& message)
{
	// create variable for input
	std::string str;
	// display given message
	std::cout << message;
	// get string from user
	std::getline(std::cin, str);

	return str;
}

char addons::get_char(std::string& message)
{
	std::string res = get_string(message);
	return res[0];
}
