#pragma once
#include <cmath>
#include "exceptions.h"

struct MathUtils
{
	static double cal_pentagon_area(double& side);
	static double cal_hexagon_area(double& side);
};
