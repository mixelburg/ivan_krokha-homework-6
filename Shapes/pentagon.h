#pragma once
#include "exceptions.h"
#include "shape.h"
#include "MathUtils.h"

class pentagon : public Shape
{
public:
	pentagon(std::string name, std::string color, double side);
	void draw();
	double CalArea() override;
	void set_side(double side);
	double get_side();
private:
	double _side;
};
